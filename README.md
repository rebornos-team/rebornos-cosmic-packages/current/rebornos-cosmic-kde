# rebornos-cosmic-kde

KDE packages

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-cosmic-packages/current/rebornos-cosmic-kde.git
```

# Arch Linux changes (2020.06.13):

**kdeutils** change to **kde-utilities-meta**

**kde-system-meta** replaces **kdeadmin-meta** and **kdebase-meta**
<br><br><br>

**kde-system-meta**:

dolphin<br>
kcron<br>
khelpcenter<br>
ksystemlog<br>
<br><br>


**kde-utilities-meta**:

ark<br>
filelight<br>
kate<br>
kbackup<br>
kcalc<br>
kcharselect<br>
kdebugsettings<br>
kdf<br>
kdialog<br>
keditbookmarks<br>
kfind<br>
kfloppy<br>
kgpg<br>
konsole<br>
kteatime<br>
ktimer<br>
kwalletmanager<br>
kwrite<br>
print-manager<br>
sweeper<br>
yakuake<br>
<br><br>


**meta-plasma** content without discover:

bluedevil<br>
drkonqi<br>
kde-gtk-config<br>
kdeplasma-addons<br>
khotkeys<br>
kinfocenter<br>
kscreen<br>
ksshaskpass<br>
kwrited<br>
oxygen<br>
plasma-browser-integration<br>
plasma-desktop<br>
plasma-disks<br>
plasma-firewall<br>
plasma-nm<br>
plasma-workspace-wallpapers<br>
plasma-pa<br>
plasma-sdk<br>
plasma-systemmonitor<br>
plasma-thunderbolt<br>
plasma-vault<br>
kwayland-integration<br>
kwallet-pam<br>
kgamma5<br>
sddm-kcm<br>
breeze-gtk<br>
powerdevil<br>
xdg-desktop-portal-kde<br>
<br>
<br>

# Arch Linux changes (2020.08.18):

Once KDE is installed, you only see the cursor on a black screen.

Added to rebornos-cosmic-kde:

**xorg-twm**, **xorg-xclock**, and **xterm**


